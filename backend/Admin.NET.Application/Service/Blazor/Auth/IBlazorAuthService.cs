﻿using Microsoft.AspNetCore.Mvc;

namespace Admin.NET.Application
{
    public interface IBlazorAuthService
    {
        Task<LoginOutput> GetLoginUserAsync();
    }
}