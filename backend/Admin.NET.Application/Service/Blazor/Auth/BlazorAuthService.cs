﻿using Admin.NET.Core;
using Admin.NET.Core.Options;
using Furion;
using Furion.DatabaseAccessor;
using Furion.DataEncryption;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.EventBus;
using Furion.FriendlyException;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using UAParser;

namespace Admin.NET.Application
{
    public class BlazorAuthService : IBlazorAuthService, IDynamicApiController, ITransient
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        private readonly IRepository<SysUser> _sysUserRep; // 用户表仓储
        private readonly ISysUserService _sysUserService; // 系统用户服务
        private readonly ISysEmpService _sysEmpService; // 系统员工服务
        private readonly ISysRoleService _sysRoleService; // 系统角色服务
        private readonly ISysMenuService _sysMenuService; // 系统菜单服务
        private readonly ISysAppService _sysAppService; // 系统应用服务
        private readonly IClickWordCaptcha _captchaHandle; // 验证码服务
        private readonly ISysConfigService _sysConfigService; // 验证码服务
        private readonly IEventPublisher _eventPublisher;

        public BlazorAuthService(IRepository<SysUser> sysUserRep, IHttpContextAccessor httpContextAccessor,
            ISysUserService sysUserService, ISysEmpService sysEmpService, ISysRoleService sysRoleService,
            ISysMenuService sysMenuService, ISysAppService sysAppService, IClickWordCaptcha captchaHandle,
            ISysConfigService sysConfigService, IEventPublisher eventPublisher)
        {
            _sysUserRep = sysUserRep;
            _httpContextAccessor = httpContextAccessor;
            _sysUserService = sysUserService;
            _sysEmpService = sysEmpService;
            _sysRoleService = sysRoleService;
            _sysMenuService = sysMenuService;
            _sysAppService = sysAppService;
            _captchaHandle = captchaHandle;
            _sysConfigService = sysConfigService;
            _eventPublisher = eventPublisher;
        }

        /// <summary>
        /// 获取当前登录用户信息
        /// </summary>
        /// <returns></returns>
        public async Task<LoginOutput> GetLoginUserAsync()
        {
            var user = _sysUserRep.FirstOrDefault(u => u.Id == CurrentUserInfo.UserId, false);
            if (user == null)
                throw Oops.Oh(ErrorCode.D1011);
            var userId = user.Id;

            var httpContext = _httpContextAccessor.HttpContext;
            var loginOutput = user.Adapt<LoginOutput>();

            loginOutput.LastLoginTime = user.LastLoginTime = DateTimeOffset.Now;
            loginOutput.LastLoginIp = user.LastLoginIp = httpContext.GetRequestIPv4();

            //var ipInfo = IpTool.Search(loginOutput.LastLoginIp);
            //loginOutput.LastLoginAddress = ipInfo.Country + ipInfo.Province + ipInfo.City + "[" + ipInfo.NetworkOperator + "][" + ipInfo.Latitude + ipInfo.Longitude + "]";

            var client = Parser.GetDefault().Parse(httpContext.Request.Headers["User-Agent"]);
            loginOutput.LastLoginBrowser = client.UA.Family + client.UA.Major;
            loginOutput.LastLoginOs = client.OS.Family + client.OS.Major;

            // 员工信息
            loginOutput.LoginEmpInfo = await _sysEmpService.GetEmpInfo(userId);

            // 角色信息
            loginOutput.Roles = await _sysRoleService.GetUserRoleList(userId);

            // 权限信息
            loginOutput.Permissions = await _sysMenuService.GetLoginPermissionList(userId);

            // 系统所有权限信息
            loginOutput.AllPermissions = await _sysMenuService.GetAllPermissionList();

            // 数据范围信息(机构Id集合)
            loginOutput.DataScopes = await _sysUserService.GetUserDataScopeIdList(userId);

            // 具备应用信息（多系统，默认激活一个，可根据系统切换菜单）,返回的结果中第一个为激活的系统
            loginOutput.Apps = await _sysAppService.GetLoginApps(userId);

            // 菜单信息
            if (loginOutput.Apps.Count > 0)
            {
                var activeApp = loginOutput.Apps.FirstOrDefault(u => u.Active == YesOrNot.Y.ToString());
                var defaultActiveAppCode = activeApp != null ? activeApp.Code : loginOutput.Apps.FirstOrDefault().Code;
                loginOutput.Menus = await _sysMenuService.GetLoginMenusAntDesign(userId, defaultActiveAppCode);
            }

            // 更新用户最后登录Ip和时间
            await _sysUserRep.UpdateIncludeAsync(user, new[] { nameof(SysUser.LastLoginIp), nameof(SysUser.LastLoginTime) });

            // 增加登录日志
            await _eventPublisher.PublishAsync(new ChannelEventSource("Create:VisLog",
                new SysLogVis
                {
                    Name = loginOutput.Name,
                    Success = YesOrNot.Y,
                    Message = "登录成功",
                    Ip = loginOutput.LastLoginIp,
                    Browser = loginOutput.LastLoginBrowser,
                    Os = loginOutput.LastLoginOs,
                    VisType = LoginType.LOGIN,
                    VisTime = loginOutput.LastLoginTime,
                    Account = loginOutput.Account
                }));
            return loginOutput;
        }
    }
}