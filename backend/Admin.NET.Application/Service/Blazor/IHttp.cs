﻿using Furion.RemoteRequest;
using Furion.UnifyResult;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application
{
    public interface IHttp : IHttpDispatchProxy
    {
        [Post("login"), Client("api")]
        Task<RESTfulResult<string>> LoginAsync([Body] LoginInput input);

        [Get("getLoginUser"), Client("api")]
        Task<LoginOutput> GetLoginUserAsync();
    }
}
