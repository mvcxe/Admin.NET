﻿namespace Masa.Blazor.Pro.Data.Shared.Favorite
{
    public static class FavoriteService
    {
        public static List<long> GetDefaultFavoriteMenuList() => new() { 5, 2, 15 };
    }
}
