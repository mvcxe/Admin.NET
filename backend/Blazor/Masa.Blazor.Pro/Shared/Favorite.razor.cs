﻿namespace Masa.Blazor.Pro.Shared
{
    public partial class Favorite
    {
        [Inject]
        private ISysMenuService? Service { get; set; }


        private async Task ChangeSysMenu(string Code)
        {
            var menus = await Service.GetLoginMenusAntDesign(UserInfo.Id, Code);
            UserInfo.Menus = menus;
            NavHelper.Init(menus);
            StateHasChanged();
            //await InvokeAsync(StateHasChanged);
            //await InvokeAsync(() => StateHasChanged());
        }
    }
}
