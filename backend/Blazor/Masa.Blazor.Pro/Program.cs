
using Admin.NET.Application;
using Admin.NET.Web.Core;

var builder = WebApplication.CreateBuilder(args).Inject();

builder.Host.UseSerilogDefault().ConfigureAppConfiguration((hostingContext, config) =>
{
    config.AddJsonFile("applicationsettings.json", optional: true, reloadOnChange: true);
});
// 工作流注册
builder.Services.AddWorkflow(options =>
{
    options.UsePersistence(sp => sp.GetService<FurionPersistenceProvider>());
});
// 工作流JSON注册
builder.Services.AddWorkflowDSL();

// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();
builder.Services.AddMasaBlazor(builder =>
{
    builder.Theme.Primary = "#4318FF";
    builder.Theme.Accent = "#4318FF";
}).AddI18nForServer("wwwroot/i18n");
builder.Services.AddHttpContextAccessor();
builder.Services.AddGlobalForServer();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
}
else
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();
app.MapBlazorHub();
app.MapFallbackToPage("/_Host");

// 工作流注入
app.UseWorkflow();

app.Run();
