﻿using Admin.NET.Core;
using Admin.NET.Core.Service;

namespace Masa.Blazor.Pro.Sys.App
{
    public partial class Index
    {
        [Inject]
        private ISysAppService? Service { get; set; }

        private string? Name { get; set; }
        private string? Code { get; set; }
        private string? Search { get; set; }

        private bool AddVisible;
        private bool EditVisible;
        private UpdateAppInput EditItem = new UpdateAppInput();

        private PageResult<SysApp> Page = new PageResult<SysApp> { Rows = new List<SysApp>() };
        private readonly List<DataTableHeader<SysApp>> Headers = new()
        {
            new() { Text = "Name", Value = nameof(SysApp.Name), CellClass = "" },
            new() { Text = "Code", Value = nameof(SysApp.Code) },
            new() { Text = "Active", Value = nameof(SysApp.Active) },
            new() { Text = "Status", Value = nameof(SysApp.Status) },
            new() { Text = "Sort", Value = nameof(SysApp.Sort) },
            new() { Text = "Actions", Value = "Action", Sortable = false }
        };

        private async Task Delete(long id)
        {
            var confirmed = await PopupService.ConfirmAsync("应用", "确认删除？");
            await handleDelete(confirmed, id);
        }

        private async Task ChangeStatus(SysApp data)
        {
            ChangeUserAppStatusInput input = new ChangeUserAppStatusInput();
            input.Id = data.Id;
            bool confirmed;
            if (data.Status == CommonStatus.ENABLE)
            {
                input.Status = CommonStatus.DISABLE;
                confirmed = await PopupService.ConfirmAsync("应用", "确定停用该应用？");
            }
            else
            {
                input.Status = CommonStatus.ENABLE;
                confirmed = await PopupService.ConfirmAsync("应用", "确定启用该应用？");
            }
            await handleChangeStatus(confirmed, input);
        }

        private async Task SetAsDefault(long id)
        {
            var confirmed = await PopupService.ConfirmAsync("应用", "设置为默认应用？");
            await handleSetAsDefault(confirmed, id);
        }

        private async Task Edit(SysApp data)
        {
            EditItem.Id = data.Id;
            EditItem.Code = data.Code;
            EditItem.Name = data.Name;
            EditItem.Sort = data.Sort;
            EditVisible = true;
        }

        private async Task handleDelete(bool confirmed, long id)
        {
            if (confirmed)
            {
                await Service.DeleteApp(new BaseId() { Id = id });
            }
        }

        private async Task handleChangeStatus(bool confirmed, ChangeUserAppStatusInput data)
        {
            if (confirmed)
            {
                await Service.ChangeUserAppStatus(data);
            }
        }

        private async Task handleSetAsDefault(bool confirmed, long id)
        {
            if (confirmed)
            {
                await Service.SetAsDefault(new SetDefaultAppInput() { Id = id });
            }
        }

        private async Task handleAdd(AddAppInput data)
        {
            data.Active = "N";
            await Service.AddApp(data);
        }

        private async Task handleEdit(UpdateAppInput data)
        {
            await Service.UpdateApp(data);
        }

        public List<SysApp> GetPageDatas()
        {
            AddVisible = false;
            EditVisible = false;
            AppPageInput input = new AppPageInput();
            if (Search is not null)
            {
                input.Name = Search;
                input.Code = Search;
            }

            if (Name is not null)
            {
                input.Name = Name;
            }

            if (Code is not null)
            {
                input.Code = Code;
            }
            Page = Service.QueryAppPageList(input).GetAwaiter().GetResult();
            return Page.Rows.ToList();
        }
    }
}
