﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Primitives;
using Microsoft.JSInterop;
using Microsoft.JSInterop.Infrastructure;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Masa.Blazor.Pro.Global
{
    public abstract class BasePage : ComponentBase
    {
        [Inject]
        public IJSRuntime? JSRuntime { get; set; }

        [Inject]
        public IHttpContextAccessor? httpContextAccessor { get; set; }

        [CascadingParameter]
        public LoginOutput UserInfo
        {
            get;
            set;
        }

        //
        // 摘要:
        //     日期类型的 Claim 类型
        private static readonly string[] DateTypeClaimTypes = new string[3] { "iat", "nbf", "exp" };

        public async Task<string> GetToken()
        {
            return await GetLocalStorage<string>("masatoken");
        }
        public async Task<string> GetRefreshToken()
        {
            return await GetLocalStorage<string>("masarefreshtoken");
        }

        public async Task SetToken(string token, string refreshtoken)
        {
            await SetLocalStorage("masatoken", token);
            await SetLocalStorage("masarefreshtoken", refreshtoken);
        }

        public async Task DeleteToken()
        {
            await DeleteLocalStorage("masatoken");
            await DeleteLocalStorage("masarefreshtoken");
        }

        public async Task SetUserInfo(LoginOutput userinfo)
        {
            await JSRuntime.InvokeVoidAsync("localStorageFuncs.set", "masauser", JsonSerializer.Serialize(userinfo));
        }

        public async Task<int> localClient()
        {
            return await JSRuntime.InvokeAsync<int>("localClient.getTimeZone", null);
        }

        public async Task<LoginOutput> GetUserInfo()
        {
            var user = await GetLocalStorage<LoginOutput>("masauser");
            JsonSerializerOptions options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            //user.Attributes["Actions"] = JsonSerializer.Deserialize<string[]>(user.Attributes["Actions"].ToString(), options).Where(x => x != null).ToArray();
            //user.Attributes["Menus"] = JsonSerializer.Deserialize<SimpleMenuApi[]>(user.Attributes["Menus"].ToString(), options);
            return user;
        }


        public async Task<T> GetLocalStorage<T>(string key) where T : class
        {
            string rv = "";
            while (true)
            {
                string part = await JSRuntime.InvokeAsync<string>("localStorageFuncs.get", System.Threading.CancellationToken.None, key, rv.Length);
                if (part == null)
                {
                    return null;
                }
                rv += part;
                if (part.Length < 20000)
                {
                    break;
                }
            }
            if (typeof(T) == typeof(string))
            {
                return rv as T;
            }
            var obj = JsonSerializer.Deserialize<T>(rv);
            return obj;
        }
        public async Task SetLocalStorage<T>(string key, T data) where T : class
        {
            if (typeof(T) == typeof(string))
            {
                await JSRuntime.InvokeVoidAsync("localStorageFuncs.set", key, data);
            }
            else
            {
                await JSRuntime.InvokeVoidAsync("localStorageFuncs.set", key, JsonSerializer.Serialize(data));
            }
        }
        public async Task DeleteLocalStorage(string key)
        {
            await JSRuntime.InvokeAsync<string>("localStorageFuncs.remove", key);
        }

        //
        // 摘要:
        //     通过过期Token 和 刷新Token 换取新的 Token
        //
        // 参数:
        //   expiredToken:
        //
        //   refreshToken:
        //
        //   expiredTime:
        //     过期时间（分钟）
        //
        //   clockSkew:
        //     刷新token容差值，秒做单位
        private string Exchange(string expiredToken, string refreshToken, long? expiredTime = null, long clockSkew = 5L)
        {
            if (Furion.DataEncryption.JWTEncryption.Validate("Bearer " + expiredToken).IsValid)
            {
                return null;
            }

            var (flag, jsonWebToken, _) = Furion.DataEncryption.JWTEncryption.Validate(refreshToken);
            if (!flag)
            {
                return null;
            }

            //HttpContext currentHttpContext = GetCurrentHttpContext();
            /*string key = "BLACKLIST_REFRESH_TOKEN:" + refreshToken;
            IDistributedCache distributedCache = httpContextAccessor.HttpContext?.RequestServices?.GetService<IDistributedCache>();
            DateTimeOffset utcNow = DateTimeOffset.UtcNow;
            string text = distributedCache?.GetString(key);
            bool flag2 = !string.IsNullOrWhiteSpace(text);
            if (flag2)
            {
                DateTimeOffset dateTimeOffset = new DateTimeOffset(long.Parse(text), TimeSpan.Zero);
                if ((utcNow - dateTimeOffset).TotalSeconds > (double)clockSkew)
                {
                    return null;
                }
            }*/

            string[] array = expiredToken.Split('.', StringSplitOptions.RemoveEmptyEntries);
            if (array.Length < 3)
            {
                return null;
            }

            if (!jsonWebToken.GetPayloadValue<string>("f").Equals(array[0]))
            {
                return null;
            }

            if (!jsonWebToken.GetPayloadValue<string>("e").Equals(array[2]))
            {
                return null;
            }

            if (!array[1].Substring(jsonWebToken.GetPayloadValue<int>("s"), jsonWebToken.GetPayloadValue<int>("l")).Equals(jsonWebToken.GetPayloadValue<string>("k")))
            {
                return null;
            }

            JwtPayload payload = Furion.DataEncryption.JWTEncryption.SecurityReadJwtToken(expiredToken).Payload;
            string[] dateTypeClaimTypes = DateTypeClaimTypes;
            foreach (string key2 in dateTypeClaimTypes)
            {
                if (payload.ContainsKey(key2))
                {
                    payload.Remove(key2);
                }
            }

            /*if (!flag2)
            {
                distributedCache?.SetString(key, utcNow.Ticks.ToString(), new DistributedCacheEntryOptions
                {
                    AbsoluteExpiration = DateTimeOffset.FromUnixTimeSeconds(jsonWebToken.GetPayloadValue<long>("exp"))
                });
            }*/

            return Furion.DataEncryption.JWTEncryption.Encrypt(payload, expiredTime);
        }

        public async Task<bool> AutoRefreshToken(long? expiredTime = null, int refreshTokenExpiredTime = 43200, string tokenPrefix = "Bearer ", long clockSkew = 5L)
        {
            if (httpContextAccessor.HttpContext.User.Identity!.IsAuthenticated)
            {
                return true;
            }

            if (httpContextAccessor.HttpContext.GetEndpoint()?.Metadata?.GetMetadata<AllowAnonymousAttribute>() != null)
            {
                return true;
            }

            var jwtBearerToken = await GetToken();
            string jwtBearerToken2 = await GetRefreshToken();
            if (string.IsNullOrWhiteSpace(jwtBearerToken) || string.IsNullOrWhiteSpace(jwtBearerToken2))
            {
                return false;
            }

            string text = Exchange(jwtBearerToken, jwtBearerToken2, expiredTime, clockSkew);
            if (string.IsNullOrWhiteSpace(text))
            {
                return false;
            }

            IEnumerable<Claim> enumerable = Furion.DataEncryption.JWTEncryption.ReadJwtToken(text)?.Claims;
            if (enumerable == null)
            {
                return false;
            }

            ClaimsIdentity claimsIdentity = new ClaimsIdentity("AuthenticationTypes.Federation");
            claimsIdentity.AddClaims(enumerable);
            ClaimsPrincipal principal = (httpContextAccessor.HttpContext.User = new ClaimsPrincipal(claimsIdentity));
            httpContextAccessor.HttpContext.SignInAsync(principal);
            //string text2 = "access-token";
            //string text3 = "x-access-token";
            //string key = "Access-Control-Expose-Headers";
            //httpContextAccessor.HttpContext.Response.Headers[text2] = jwtBearerToken;
            //httpContextAccessor.HttpContext.Response.Headers[text3] = Furion.DataEncryption.JWTEncryption.GenerateRefreshToken(jwtBearerToken, refreshTokenExpiredTime);
            //httpContextAccessor.HttpContext.Response.Headers.TryGetValue(key, out var value);
            //httpContextAccessor.HttpContext.Response.Headers[key] = string.Join(',', StringValues.Concat(value, new StringValues(new string[2] { text2, text3 })).Distinct());
            return true;
        }
    }
}
