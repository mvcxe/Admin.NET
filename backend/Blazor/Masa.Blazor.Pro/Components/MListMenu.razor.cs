﻿namespace Masa.Blazor.Pro.Components;

public partial class MListMenu
{
    [Parameter]
    public AntDesignTreeNode? Menu { get; set; }

    private bool hasChild { 
        get {
            foreach (var menu in UserInfo.Menus)
            {
                if (menu.Pid == Menu.Id)
                {
                    return true;
                }
            }
            return false;
        } 
    }
}

